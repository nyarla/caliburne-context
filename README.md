caliburne-context
=================

  * A manager for caliburne.js context object.

Description
-----------

This library is a manager for [caliburne.js](https://github.com/nyarla/caliburne) context object.

Examples
--------

```js
import createContext from 'caliburne-context';

const MyAwesomeMiddleware = {
  // this function is passed to caliburne context inintializer.
  middleware(state) {
    // this function should returns new state object.
    return state;
  },

  // this function is called when caliburne context object was created.
  bind(context) {
    // `context` is a context object for caliburne.js
    // this function must return extended context object.
    return context;
  }
};

const initialState = {};

const context = createContext(initialState, [
  MyAwesomeMiddleware
]);

// write your code is here.

```

Author
------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

License
-------

MIT

