import caliburne from 'caliburne';

export default function (initialState = {}, middlewares = []) {
  let [ Middlewares, Binders ] = [ [], [] ];
 
  middlewares.forEach((middleware) => {
    if ( typeof(middleware.middleware) === 'function' ) {
      Middlewares.push(middleware.middleware);
    }

    if ( typeof(middleware.bind) === 'function' ) {
      Binders.push(middleware.bind);
    }
  });

  let context = caliburne(initialState, Middlewares);
  
  Binders.forEach((binder) => {
    context = binder(context);
  });

  return context;
}

