import assert from 'assert';
import createContext from '../src/caliburne-context';

let testMiddleware = {
  middleware(state) {
    state.test = true;
    return state;
  },

  bind(context) {
    context.test = true;
    return context;
  }
}

const initialState  = {};
const context       = createContext(initialState, [ testMiddleware ]);

assert( context.test === true );

context.on(':apply-updating', (state) => {
  assert( state.test === true );
  return state;
});

context.update((_state) => {
  return initialState;
});
